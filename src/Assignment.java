import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment {
    private JPanel root;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane OrderedItemsList;
    private JButton checkOutButton;
    private JLabel TotalPrice;

    int total = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price){
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
            String beforeorder = OrderedItemsList.getText();
            OrderedItemsList.setText( beforeorder + food + "\n");
            total += price;
            TotalPrice.setText(total + " yen");
        }
    }

    public Assignment() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 100);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 100);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 100);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 100);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 100);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 100);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + total + " yen.");
                    total = 0;
                    TotalPrice.setText(total + " yen");
                    OrderedItemsList.setText("");
                }
            }
        });
    }
}
