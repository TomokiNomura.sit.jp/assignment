import java.util.Scanner;

public class Method {
    static void order(String food) {
        System.out.println("You have ordered " + food + ". Thank you!");
    }

    public static void main(String[] args) {
        System.out.println("What would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        Scanner userIn = new Scanner(System.in);
        System.out.print("Your order [1-3]: ");
        int number = userIn.nextInt();
        userIn.close();
        if (number == 1) {
            order("Tempura");
        } else if (number == 2) {
            order("Ramen");
        } else if (number == 3) {
            order("Udon");
        }
    }
}
